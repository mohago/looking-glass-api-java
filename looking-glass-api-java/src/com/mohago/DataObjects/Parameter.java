package com.mohago.DataObjects;

class Parameter<T> extends PBase {
	
	 private T p_value;

	Parameter(String name, T value, int parameterIndex,
			LookingGlassType precision) {
		this.setP_name(name);
		this.setP_value(value);
		this.setParameterIndex(parameterIndex);
		this.setParameterType(precision);
	}

	T getP_value() {
		return p_value;
	}

	void setP_value(T p_value) {
		this.p_value = p_value;
	}

}
