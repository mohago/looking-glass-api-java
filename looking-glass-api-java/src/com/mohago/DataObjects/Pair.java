package com.mohago.DataObjects;

class Pair<T,U> {
	
	private T first;
    private U second;
    Pair(T first, U second)
    {
        this.first = first;
        this.second = second;
    }
	T getFirst() {
		return first;
	}
	void setFirst(T first) {
		this.first = first;
	}
	U getSecond() {
		return second;
	}
	void setSecond(U second) {
		this.second = second;
	}

}
