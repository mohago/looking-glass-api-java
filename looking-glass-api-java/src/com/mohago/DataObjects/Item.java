package com.mohago.DataObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

/**
 * Data object which is made up of name-value "Parameters". Items are built up with parameters, and then added to DataSets
 * 
 * @author dylan.buckley@mohago.com
 */
class Item {
	
	 private ArrayList<Parameter<Integer>> parameterInteger = new ArrayList<Parameter<Integer>>();
     private ArrayList<Parameter<Double>> parameterDouble = new ArrayList<Parameter<Double>>();
     private ArrayList<Parameter<String>> parameterString = new ArrayList<Parameter<String>>();
     private ArrayList<Parameter<Date>> parameterDate = new ArrayList<Parameter<Date>>();

    ArrayList<Parameter<Integer>> getParameterInteger() {
		return parameterInteger;
	}
	void setParameterInteger(ArrayList<Parameter<Integer>> parameterInteger) {
		this.parameterInteger = parameterInteger;
	}
	ArrayList<Parameter<Double>> getParameterDouble() {
		return parameterDouble;
	}
	void setParameterDouble(ArrayList<Parameter<Double>> parameterDouble) {
		this.parameterDouble = parameterDouble;
	}
	ArrayList<Parameter<String>> getParameterString() {
		return parameterString;
	}
	void setParameterString(ArrayList<Parameter<String>> parameterString) {
		this.parameterString = parameterString;
	}
	ArrayList<Parameter<Date>> getParameterDate() {
		return parameterDate;
	}
	void setParameterDate(ArrayList<Parameter<Date>> parameterDate) {
		this.parameterDate = parameterDate;
	}
	HashSet<String> getParameterNames() {
		return parameterNames;
	}
	void setParameterNames(HashSet<String> parameterNames) {
		this.parameterNames = parameterNames;
	}
	HashSet<LookingGlassType> getParameterTypes() {
		return parameterTypes;
	}
	void setParameterTypes(HashSet<LookingGlassType> parameterTypes) {
		this.parameterTypes = parameterTypes;
	}
	int parameterIndex = 0;

     private HashSet<String> parameterNames = new HashSet<String>();
     
     private HashSet<LookingGlassType> parameterTypes = new HashSet<LookingGlassType>();
     
     private ArrayList<PBase> allParameters;

     ArrayList<PBase> getAllParameters() {
    	 ArrayList<PBase> ret = new ArrayList<PBase>();
    	 for(Parameter p : parameterDouble){
    		 ret.add(p);
    	 }
    	 for(Parameter p : parameterInteger){
    		 ret.add(p);
    	 }
    	 for(Parameter p : parameterString){
    		 ret.add(p);
    	 }
    	 for(Parameter p : parameterDate){
    		 ret.add(p);
    	 }
         return ret;
	}
	void setAllParameters(ArrayList<PBase> allParameters) {
		this.allParameters = allParameters;
	}

         ///No duplication of parameter names is allowed.

    

	/**
	 * Adds a double type parameter to this item.
	 * @param name The name of the parameter
	 * @param value The value of the parameter
	 * @deprecated
	 */
     boolean addParameterDouble(String Name, double Value)
     {

    	 for(String name : parameterNames){
    		 if(name.equals(Name)){
    			 return false;
    		 }
    	 }


             parameterDouble.add(new Parameter<Double>(Name, Value, parameterIndex, LookingGlassType.double_precision));
             parameterIndex++; 
             parameterNames.add(Name); 
             parameterTypes.add(LookingGlassType.double_precision);
             return true;
     }
     /**
 	 * Adds a double type parameter to this item.
 	 * @param name The name of the parameter
 	 * @param value The value of the parameter
 	 * @deprecated
 	 */
     boolean addParameterString(String Name, String Value)
     {
    	 for(String name : parameterNames){
    		 if(name.equals(Name)){
    			 return false;
    		 }
    	 }
    	 parameterString.add(new Parameter<String>(Name, Value, parameterIndex, LookingGlassType.string_value));
         parameterIndex++; 
         parameterNames.add(Name); 
         parameterTypes.add(LookingGlassType.string_value);
         return true;
     }
     /**
 	 * Adds a double type parameter to this item.
 	 * @param name The name of the parameter
 	 * @param value The value of the parameter
 	 * @deprecated
 	 */
     boolean addParameterDate(String Name, Date Value)
     {
    	 for(String name : parameterNames){
    		 if(name.equals(Name)){
    			 return false;
    		 }
    	 }
    	 parameterDate.add(new Parameter<Date>(Name, Value, parameterIndex, LookingGlassType.date_time));
         parameterIndex++; 
         parameterNames.add(Name); 
         parameterTypes.add(LookingGlassType.date_time);
         return true;
     }
     /**
 	 * Adds a double type parameter to this item.
 	 * @param name The name of the parameter
 	 * @param value The value of the parameter
 	 * @deprecated
 	 */
     boolean addParameterInt(String Name, int Value)
     {
    	 for(String name : parameterNames){
    		 if(name.equals(Name)){
    			 return false;
    		 }
    	 }
    	 parameterInteger.add(new Parameter<Integer>(Name, Value, parameterIndex, LookingGlassType.integer));
         parameterIndex++; 
         parameterNames.add(Name); 
         parameterTypes.add(LookingGlassType.integer);
         return true;
     }

}
