package com.mohago.DataObjects;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.xml.stream.XMLStreamException;

public interface IDataSet {
	
    void attachMeta(String name, int value) throws LookingGlassException;
    void attachMeta(String name, double value) throws LookingGlassException;
    void attachMeta(String name, String value) throws LookingGlassException;
    void attachMeta(String name, java.util.Date value) throws LookingGlassException;
    void addParameterInt(String name, int value) throws LookingGlassException;
    void addParameterDouble(String name, double value) throws LookingGlassException;
    void addParameterString(String name, String value) throws LookingGlassException;
    void addParameterDate(String name, Date value) throws LookingGlassException;
    void setName(String Name) throws LookingGlassException;
    void setDate(Date date);
    void setParentDatasetId(Long parentUid) throws LookingGlassException;
    Long getDatasetId();
    void setDatasetId(Long id);
    void setFolderPath(String FolderPath);
    void endItem() throws IOException;
    String endWrite() throws IOException, XMLStreamException, NoSuchAlgorithmException, LookingGlassException;
    String stream() throws LookingGlassException, IOException, NoSuchAlgorithmException, XMLStreamException;
    void writeCSV(String path) throws FileNotFoundException, IOException;


}
