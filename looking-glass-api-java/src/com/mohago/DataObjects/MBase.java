package com.mohago.DataObjects;

class MBase {
	
	private String p_name;
    private LookingGlassType parameterType;
    Object returnValue(){
        return null;
    }
	String getP_name() {
		return p_name;
	}
	void setP_name(String p_name) {
		this.p_name = p_name;
	}
	LookingGlassType getParameterType() {
		return parameterType;
	}
	void setParameterType(LookingGlassType parameterType) {
		this.parameterType = parameterType;
	}

}
