package com.mohago.DataObjects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

class ParameterCollection<T> extends PCBase {

	private ArrayList<Pair<Boolean, T>> p_values = new ArrayList<Pair<Boolean, T>>();

	private ArrayList<Pair<Integer, T>> p_indexedvalues = new ArrayList<Pair<Integer, T>>();

	
	ParameterCollection(String name, LookingGlassType parameterType, int parameterIndex){
		this.setP_name(name);
		this.setParameterType(parameterType);
		this.setParameterIndex(parameterIndex);
		
	}
	// / <summary>
	// / add value from external source with error checking on null, maybe
	// extend to parse.
	// / </summary>
	// / <param name="index"></param>
	// / <param name="newValue"></param>
	void AddValue(int index, Object newValue) {

		if (this.getParameterType() == LookingGlassType.double_precision) {
			try {
				double r = Double.valueOf((String) newValue);
				p_indexedvalues
						.add((Pair<Integer, T>) new Pair<Integer, Double>(
								index, r));
			} catch (NullPointerException npe) {

			}
		} else if (this.getParameterType() == LookingGlassType.integer) {
			int r;
			try {
				r = Integer.valueOf((String) newValue);
				p_indexedvalues
						.add((Pair<Integer, T>) new Pair<Integer, Integer>(
								index, r));
			} catch (NullPointerException npe) {

			}
		} else if (this.getParameterType() == LookingGlassType.date_time) {
			Date r;
			r = new Date((String) newValue);
			p_indexedvalues.add((Pair<Integer, T>) new Pair<Integer, Date>(
					index, r));

		} else if (this.getParameterType() == LookingGlassType.string_value) {
			String r;
			r = (String) newValue;
			p_indexedvalues.add((Pair<Integer, T>) new Pair<Integer, String>(
					index, r));
		}
	}

	ArrayList<Pair<Boolean, T>> getP_values() {
		return p_values;
	}

	void setP_values(ArrayList<Pair<Boolean, T>> p_values) {
		this.p_values = p_values;
	}

	ArrayList<Pair<Integer, T>> getP_indexedvalues() {
		return p_indexedvalues;
	}

	void setP_indexedvalues(ArrayList<Pair<Integer, T>> p_indexedvalues) {
		this.p_indexedvalues = p_indexedvalues;
	}

	int getCount() {
		int ret = 0;
		if (p_values != null) {
			ret = p_values.size();
		}
		return ret;
	}

	void setCount(int count) {
		this.count = count;
	}

	int getMaxIndex() {// Verify logic with dec

		if (p_indexedvalues != null && p_indexedvalues.size() > 0) {
			int i = 0;
			int maxIndex = -1;
			int max = 0;
			for (Pair<Integer, T> p : p_indexedvalues) {
				if (p.getFirst() > max) {
					max = (Integer) p.getFirst();
					maxIndex = i;
				}
				i++;
			}
		}

		return maxIndex;
	}

	void setMaxIndex(int maxIndex) {
		this.maxIndex = maxIndex;
	}

	private int count;
	private int maxIndex;
	
	// / <summary>
	// / Return value at index. No error checking for speed, assumes all
	// parameter collections are same length.
	// / </summary>
	// / <param name="index"></param>
	// / <returns></returns>
	Object ValueAt(int index) {
		Pair pair = (Pair) p_values.toArray()[index];
		if ((boolean) pair.getFirst()) {
			return pair.getSecond();
		} else {
			return null;
		}
	}

	String StringValueAt(int index) {
		String ret = "";
		Pair pair = (Pair) p_values.toArray()[index];
		if ((boolean) pair.getFirst() && pair.getSecond() != null) {
			
			
			if(getParameterType().equals(LookingGlassType.date_time)){
				
				//Date date = sdf.parse((String.valueOf((pair.getSecond()))));
				Date date = (Date)pair.getSecond();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
				ret = sdf.format(date);
			}else{
				ret = String.valueOf((pair.getSecond()));
			}
		} 
		return ret;
	}

	void PadValue() {

		p_values.add((new Pair<Boolean, T>(false, null)));
	}
	
	 void ClearValues()
     {
         p_values.clear();
     }
}
