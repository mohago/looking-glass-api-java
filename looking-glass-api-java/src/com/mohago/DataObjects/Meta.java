package com.mohago.DataObjects;

class Meta<T> extends MBase {
	
	 private T p_value;

     T getP_value() {
		return p_value;
	}

	void setP_value(T p_value) {
		this.p_value = p_value;
	}

	Object returnValue()
     {
         return p_value;
     }
	
	Meta(String name, T value, LookingGlassType lgType){
		this.setP_name(name);
		this.setP_value(value);
		this.setParameterType(lgType);
	}

}
