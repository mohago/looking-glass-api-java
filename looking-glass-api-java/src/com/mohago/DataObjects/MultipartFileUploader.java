package com.mohago.DataObjects;

import java.io.File;
import java.io.IOException;
import java.util.List;

class MultipartFileUploader {

	static boolean upload(String metaPath, String uploadUrl,
			String userName, String password) {

		boolean ret = false;
		String charset = "UTF-8";
		File uploadFile1 = new File(metaPath);
		File uploadFile2 = new File(metaPath.replace(".lgmf", ".lgdf"));
		String requestURL = uploadUrl;

		try {
			MultipartUtility multipart = new MultipartUtility(requestURL,
					charset, userName, password);

			multipart.addHeaderField("user_name", userName);
			multipart.addHeaderField("password", password);
			multipart.addFilePart("lgmf", uploadFile1);
			multipart.addFilePart("lgdf", uploadFile2);
			List<String> response = multipart.finish();
			ret = true;

			for (String line : response) {

			}
		} catch (IOException e) {
			ret = false;
		}

		return ret;
	}

}
