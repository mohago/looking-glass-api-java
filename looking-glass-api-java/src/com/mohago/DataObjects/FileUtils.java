package com.mohago.DataObjects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class FileUtils {

	static boolean isNullOrEmpty(String testString) {
		boolean ret = false;
		if (testString == null || testString.length() < 1) {
			ret = true;
		}
		return ret;
	}

	static boolean isFileEmpty(String filePath) {

		boolean ret = false;
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			ret = br.readLine() == null;
		} catch (IOException e) {
			ret = true;
		}
		return ret;
	}

	static boolean isNix() {
		String OS = System.getProperty("os.name").toLowerCase();
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS
				.indexOf("aix") > 0);
	}

	static boolean isMac() {
		String OS = System.getProperty("os.name").toLowerCase();
		return (OS.indexOf("mac") >= 0);
	}

	static boolean isWindows() {
		String OS = System.getProperty("os.name").toLowerCase();
		return (OS.indexOf("win") >= 0);
	}

	static String getFolderPath() throws IOException {

		boolean folderExists = false;
		String folderPath = "";
		if (isNix()) {
			
			String userHomePath = System.getProperty("user.home");
			folderPath = userHomePath + File.separatorChar + ".Mohago" + File.separatorChar + "Lgdf";
			folderExists = Files.exists(Paths.get(folderPath));
			if (!folderExists || FileUtils.isNullOrEmpty(folderPath)) {
				File newDir = null;
				newDir = new File(folderPath).getCanonicalFile();
				if (!newDir.isDirectory()) {
					newDir.mkdir();
				}

			}

		} else if (isMac()) {
			
			String userHomePath = System.getProperty("user.home");
			folderPath = userHomePath + "/Library/Application Support" + File.separatorChar + ".Mohago" + File.separatorChar + "Lgdf";
			folderExists = Files.exists(Paths.get(folderPath));
			if (!folderExists || FileUtils.isNullOrEmpty(folderPath)) {
				File newDir = null;
				newDir = new File(folderPath).getCanonicalFile();
				if (!newDir.isDirectory()) {
					newDir.mkdir();
				}

			}

		} else if (isWindows()) {
			String programDataPath = System.getenv("ALLUSERSPROFILE");
			folderPath = programDataPath + "\\Mohago\\Lgdf";
			folderExists = Files.exists(Paths.get(folderPath));
			if (!folderExists || FileUtils.isNullOrEmpty(folderPath)) {
				File newDir = null;
				newDir = new File(folderPath).getCanonicalFile();
				if (!newDir.isDirectory()) {
					newDir.mkdir();
				}

			}
		}

		return folderPath;
	}
}
