package com.mohago.DataObjects;

public class LookingGlassException extends Exception {

	    public LookingGlassException(String message) {
	        super(message);
	    }
}
