package com.mohago.DataObjects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Top level data-object, to which "Items" can be added. Also takes care of
 * writing to file.
 * 
 * @author dylan.buckley@mohago.com
 */
public class DataSet implements IDataSet {

	private Date date;
	private String name;
	private Long parentDatasetId;
	private Long id;
	private String deviceToken;
	private String project;
	private String cloudUserName;
	private String cloudPassword;
	private boolean isCloudType = false;
	private ArrayList<Meta<Integer>> metaInteger = new ArrayList<Meta<Integer>>();
	private ArrayList<Meta<Double>> metaDouble = new ArrayList<Meta<Double>>();
	private ArrayList<Meta<String>> metaString = new ArrayList<Meta<String>>();
	private ArrayList<Meta<Date>> metaDate = new ArrayList<Meta<Date>>();

	private ArrayList<ParameterCollection<Integer>> parameterCollectionInteger = new ArrayList<ParameterCollection<Integer>>();
	private ArrayList<ParameterCollection<Double>> parameterCollectionDouble = new ArrayList<ParameterCollection<Double>>();
	private ArrayList<ParameterCollection<String>> parameterCollectionString = new ArrayList<ParameterCollection<String>>();
	private ArrayList<ParameterCollection<Date>> parameterCollectionDate = new ArrayList<ParameterCollection<Date>>();
	
	private HashSet<String> parameterNames = new HashSet<String>();
	private ArrayList<PCBase> allParams = new ArrayList<PCBase>();

	private ArrayList<MBase> allMeta = new ArrayList<MBase>();
	private int itemCount = 0;

	private int parameterIndex = 0;

	private ArrayList<String> exceptionArrayList = new ArrayList<String>();
	static char[] chars = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	private static final char[] HEX_DIGITS = "0123456789ABCDEF".toCharArray();
	int maxParamCollectionCount;
	int maxIndexedParamCollectionValue;
	Random ran = new Random();

	private String folderPath = "";
	private String filePath = "";
	private String metaPath = "";

	FileOutputStream fileStream = null;
	private ArrayList<PCBase> runningList = new ArrayList<PCBase>();

	private boolean firstWrite, finalWrite, isStartStream, isParent,
			isStreamType, streamCalled, firstStream;
	private int streamIndex;

	private final static String NAME_LENGTH_ERROR = "Name of parameter must be under 255 characters in length";
	//private final static String CLOUD_URL = "https://api.mohago.com";
	private final static String CLOUD_URL = "http://dblaptop:8080/web-api/v1";
	private final static String DUPLICATE_PARAMETER_ERROR = "Parameter name already in use";

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public Long getDatasetId() {
		return id;
	}

	public void setDatasetId(Long id) {
		this.id = id;
	}

	String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	ArrayList<Meta<Integer>> getMetaInteger() {
		return metaInteger;
	}

	void setMetaInteger(ArrayList<Meta<Integer>> metaInteger) {
		this.metaInteger = metaInteger;
	}

	ArrayList<Meta<Double>> getMetaDouble() {
		return metaDouble;
	}

	void setMetaDouble(ArrayList<Meta<Double>> metaDouble) {
		this.metaDouble = metaDouble;
	}

	ArrayList<Meta<String>> getMetaString() {
		return metaString;
	}

	void setMetaString(ArrayList<Meta<String>> metaString) {
		this.metaString = metaString;
	}

	ArrayList<Meta<Date>> getMetaDate() {
		return metaDate;
	}

	void setMetaDate(ArrayList<Meta<Date>> metaDate) {
		this.metaDate = metaDate;
	}

	ArrayList<ParameterCollection<Integer>> getParameterCollectionInteger() {
		return parameterCollectionInteger;
	}

	void setParameterCollectionInteger(
			ArrayList<ParameterCollection<Integer>> parameterCollectionInteger) {
		this.parameterCollectionInteger = parameterCollectionInteger;
	}

	ArrayList<ParameterCollection<Double>> getParameterCollectionDouble() {
		return parameterCollectionDouble;
	}

	void setParameterCollectionDouble(
			ArrayList<ParameterCollection<Double>> parameterCollectionDouble) {
		this.parameterCollectionDouble = parameterCollectionDouble;
	}

	ArrayList<ParameterCollection<String>> getParameterCollectionString() {
		return parameterCollectionString;
	}

	void setParameterCollectionString(
			ArrayList<ParameterCollection<String>> parameterCollectionString) {
		this.parameterCollectionString = parameterCollectionString;
	}

	ArrayList<ParameterCollection<Date>> getParameterCollectionDate() {
		return parameterCollectionDate;
	}

	void setParameterCollectionDate(
			ArrayList<ParameterCollection<Date>> parameterCollectionDate) {
		this.parameterCollectionDate = parameterCollectionDate;
	}

	ArrayList<String> getExceptionArrayList() {
		return exceptionArrayList;
	}

	void setExceptionArrayList(ArrayList<String> exceptionArrayList) {
		this.exceptionArrayList = exceptionArrayList;
	}

	// ArrayList<Pair<String, LookingGlassType>> getParameterNameTypeList() {
	// return parameterNameTypeList;
	// }

	void indexToPaddedAllCollections() {
		int max = maxIndexedParamCollectionValue;
		// allParams.ForEach(p => p.IndexedToPadded(max));
		for (PCBase pcb : this.getAllParams()) {
			pcb.IndexedToPadded(max);
		}
	}

	ArrayList<PCBase> getAllParams() {

		ArrayList<PCBase> paramArrayList = new ArrayList<PCBase>();
		for (ParameterCollection<Date> pc : this.getParameterCollectionDate()) {
			paramArrayList.add(pc);

		}
		for (ParameterCollection<String> pc : this
				.getParameterCollectionString()) {
			paramArrayList.add(pc);

		}
		for (ParameterCollection<Integer> pc : this
				.getParameterCollectionInteger()) {
			paramArrayList.add(pc);

		}
		for (ParameterCollection<Double> pc : this
				.getParameterCollectionDouble()) {
			paramArrayList.add(pc);

		}
		return paramArrayList;
	}

	void setAllParams(ArrayList<PCBase> allParams) {
		this.allParams = allParams;
	}

	int getParameterIndex() {
		return parameterIndex;
	}

	void setParameterIndex(Integer parameterIndex) {
		this.parameterIndex = parameterIndex;
	}

	static char[] getChars() {
		return chars;
	}

	static void setChars(char[] chars) {
		DataSet.chars = chars;
	}

	ArrayList<PCBase> getRunningList() {
		return runningList;
	}

	void setRunningList(ArrayList<PCBase> runningList) {
		this.runningList = runningList;
	}

	String getFolderPath() {
		return folderPath;
	}

	String getFilePath() {
		return filePath;
	}

	void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	String getMetaPath() {
		return metaPath;
	}

	void setMetaPath(String metaPath) {
		this.metaPath = metaPath;
	}

	/**
	 * Sets the Date associated with the Data-Set. This will correspond to it's
	 * position on the Looking Glass UI Time-Line.
	 * 
	 * @param date
	 *            The date associated with the creation of the Data-Set
	 * 
	 */
	public void setDate(Date date) {

		this.date = date;
	}

	/**
	 * Sets the Task that the Data-Set belongs to. In the Looking-Glass
	 * platform, Projects are subdivided into separate tasks.
	 * 
	 * @param task
	 *            The task associated with the Data-set. This will correspond to
	 *            the Project-Task delineation on the Looking Glass Dash-board
	 */
//	public void setTask(String task) {
//
//		this.task = task;
//	}

	/**
	 * Sets the name associated with the Data-Set. This will correspond to it's
	 * name in the Looking Glass UI.
	 * 
	 * @param name
	 *            The name associated with the creation of the Data-Set
	 * 
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Long getParentDatasetId() {
		return parentDatasetId;
	}

	/**
	 * @param parentUid
	 *            The Global Id of the parent Dataset of this Dataset. This
	 *            should
	 * @throws LookingGlassException
	 *             Parent global Id should only be set on a cloud-type Dataset
	 *             for appending to a parent Dataset
	 */
	public void setParentDatasetId(Long parentUid)
			throws LookingGlassException {
		this.parentDatasetId = parentUid;
		if (!this.isCloudType) {
			throw new LookingGlassException(
					"Parent global Id should only be set on a cloud-type Dataset for appending to a parent Dataset");
		}

	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	ArrayList<MBase> getAllMeta() {

		ArrayList<MBase> mList = new ArrayList<MBase>();
		for (MBase md : metaDate) {
			mList.add(md);
		}
		for (MBase md : metaString) {
			mList.add(md);
		}
		for (MBase md : metaInteger) {
			mList.add(md);
		}
		for (MBase md : metaDouble) {
			mList.add(md);
		}
		return mList;
	}

	void setAllMeta(ArrayList<MBase> allMeta) {
		this.allMeta = allMeta;
	}

	int getItemCount() {
		return itemCount;
	}

	void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	int getMaxParamCollectionCount() {

		int max = -1;
		int count = -1;
		for (PCBase p : getAllParams()) {
			count = p.getCount();
			if (count > max) {
				max = count;
			}
		}
		return max;
	}

	void setMaxParamCollectionCount(Integer maxParamCollectionCount) {
		this.maxParamCollectionCount = maxParamCollectionCount;
	}

	int getMaxIndexedParamCollectionValue() {

		int max = -1;
		int maxIndex = -1;
		for (PCBase p : this.getAllParams()) {
			maxIndex = p.getMaxIndex();
			if (maxIndex > max) {
				max = maxIndex;
			}
		}
		return max;
	}

	/**
	 * Constructor for non-stream type Datasets
	 */
	public DataSet() {
		this.setName("unnamed_API_dataset");
		date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//		String formattedDate = sdf.format(date);
//		String randomToken = createToken();
//		this.setSelfGlobalId("API" + "_" + randomToken + "_" + formattedDate);
		this.setProject("local");
//		this.task = "API import";
//		this.setSource("API_capture");
//		this.setUser("API_local");
		firstWrite = true;
	}

	/**
	 * Constructor for Datasets that be will uploaded to the Mohago cloud.
	 * 
	 * @param cloudUserName
	 *            User name for cloud credentials
	 * @param cloudPassword
	 *            Password for cloud credentials
	 */
	public DataSet(String cloudUserName, String cloudPassword) {
		isCloudType = true;
		this.cloudUserName = cloudUserName;
		this.cloudPassword = cloudPassword;
		this.setName("unnamed_API_dataset");
		date = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//		String formattedDate = sdf.format(date);
//		String randomToken = createToken();
		this.setDatasetId(new Long(ran.nextInt(Integer.MAX_VALUE)));
		try {
			this.setParentDatasetId(this.getDatasetId());
		} catch (LookingGlassException e) {
			// Nothing useful to be gained by promoting this exception
		}
		this.setProject("local");
//		this.task = "API import";
		//this.setSource("API_capture");
		//this.setUser("API_local");
		firstWrite = true;
	}

	/**
	 * Constructor for stream type Datasets
	 * 
	 * @param isStream
	 *            Whether Stream will be able to be called on this Dataset or
	 *            not
	 */
	public DataSet(boolean isStream) {
		this.setName("unnamed_API_dataset");
		date = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//		String formattedDate = sdf.format(date);
//		String randomToken = createToken();
		this.setDatasetId(new Long(ran.nextInt(Integer.MAX_VALUE)));
		this.setProject("local");
//		this.task = "API import";
//		this.setSource("API_capture");
//		this.setUser("API_local");

		if (isStream) {
			isParent = true;
			isStartStream = true;
			try {
				this.setParentDatasetId(this.getDatasetId());
			} catch (LookingGlassException e) {
				// Nothing useful to be gained by promoting this
			}
			isStreamType = true;
			streamIndex = 0;
		}

		firstWrite = true;
	}

	void setMaxIndexedParamCollectionValue(
			Integer maxIndexedParamCollectionValue) {
		this.maxIndexedParamCollectionValue = maxIndexedParamCollectionValue;
	}

	// void paddedToIndexedAll() {
	// for (PCBase pcb : allParams) {
	// pcb.PaddedToIndexed();
	// }
	// }
	//
	// // / <summary>
	// // / Use of this function not supported for datasets that have had items
	// // added using AddItem
	// // / </summary>
	// // / <param name="pc"></param>
	// void attachParameterCollection(PCBase pc) {
	// if (!parameterNames.contains(pc.getP_name())) {
	// if (pc.getParameterType() == LookingGlassType.double_precision) {
	// this.getParameterCollectionDouble().add(
	// (ParameterCollection<Double>) pc);
	// } else if (pc.getParameterType() == LookingGlassType.integer) {
	// this.getParameterCollectionInteger().add(
	// (ParameterCollection<Integer>) pc);
	// } else if (pc.getParameterType() == LookingGlassType.string_value) {
	// this.getParameterCollectionString().add(
	// (ParameterCollection<String>) pc);
	// } else if (pc.getParameterType() == LookingGlassType.date_time) {
	// this.getParameterCollectionDate().add(
	// (ParameterCollection<Date>) pc);
	// }
	// parameterNames.add(pc.getP_name());
	// }
	//
	// }

	/**
	 * Attaches a meta-data parameter of type Integer to the data-set.
	 * 
	 * @param name
	 *            The name of the meta-data parameter you are attaching.
	 * @param value
	 *            The int type value of the meta-data parameter you are
	 *            attaching to the Data-set.
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 * 
	 */
	public void attachMeta(String name, int value) throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		this.getMetaInteger().add(
				new Meta<Integer>(name, value, LookingGlassType.integer));
	}

	/**
	 * Attaches a meta-data parameter of type double to the data-set.
	 * 
	 * @param name
	 *            The name of the meta-data parameter you are attaching.
	 * @param value
	 *            The double type value of the meta-data parameter you are
	 *            attaching to the Data-set.
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 * 
	 */
	public void attachMeta(String name, double value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		this.getMetaDouble()
				.add(new Meta<Double>(name, value,
						LookingGlassType.double_precision));
	}

	/**
	 * Attaches a meta-data parameter of type String to the data-set.
	 * 
	 * @param name
	 *            The name of the meta-data parameter you are attaching.
	 * @param value
	 *            The String type value of the meta-data parameter you are
	 *            attaching to the Data-set.
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 * 
	 */
	public void attachMeta(String name, String value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		this.getMetaString().add(
				new Meta<String>(name, value, LookingGlassType.string_value));
	}

	/**
	 * Attaches a meta-data parameter of type Date to the data-set.
	 * 
	 * @param name
	 *            The name of the meta-data parameter you are attaching.
	 * @param value
	 *            The Date type value of the meta-data parameter you are
	 *            attaching to the Data-set.
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 */
	public void attachMeta(String name, Date value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		this.getMetaDate().add(
				new Meta<Date>(name, value, LookingGlassType.date_time));
	}

	void clearCollections() {
		for (PCBase a : runningList) {
			a.ClearValues();
		}
		itemCount = 0;
	}

	private String createFileName() {
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
		sb.append(dateFormat.format(new java.util.Date()));
		sb.append("_");
		for (int i = 0; i < 6; i++) {
			final char c = chars[ran.nextInt(chars.length)];
			sb.append(c);
		}
		sb.append(".lgdf");
		return (sb.toString());
	}

	private String createToken() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 6; i++) {
			final char c = chars[ran.nextInt(chars.length)];
			sb.append(c);
		}
		return (sb.toString());
	}

	/**
	 * Call this to end the current item that parameters are being added to, and
	 * move to a new Item.
	 * 
	 * @throws IOException If there is an error writing to file
	 */
	public void endItem() throws IOException {
		for (PCBase a : runningList) {
			if (!a.isNewItemThisRound()) {
				a.PadValue();
			}
			a.setNewItemThisRound(false);
		}
		if (itemCount > 100) {
			periodicWrite();
		} else {
			itemCount++;
		}
	}

	private String generateMetaString(DataSet ds) {

		StringWriter sw = new StringWriter();
		try {
			XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
			XMLStreamWriter writer = outputFactory.createXMLStreamWriter(sw);
			writer.writeStartDocument();
			writer.writeStartElement("Meta");

			ArrayList<PCBase> allParams = ds.getAllParams();
			Collections.sort(allParams, new Comparator<PCBase>() {
				@Override
				public int compare(PCBase pcb1, PCBase pcb2) {
					int ret = 0;
					if (pcb1.getParameterIndex() > pcb2.getParameterIndex()) {
						ret = 1;
					} else {
						ret = -1;
					}

					return ret;
				}
			});

			if (allParams.size() > 0) {
				writer.writeStartElement("ParameterCollections");
				for (PCBase pc : allParams) {
					writer.writeStartElement("ParamCol");
					writer.writeAttribute("Name", pc.getP_name());
					writer.writeAttribute("Type", pc.getParameterType().name());
					writer.writeAttribute("Index",
							String.valueOf(pc.getParameterIndex()));
					writer.writeEndElement();

				}
				writer.writeEndElement();
			}

			// ArrayList<MBase> allMeta = ds.allMeta;
			if (this.getAllMeta().size() > 0) {
				for (MBase m : this.getAllMeta()) {
					writer.writeStartElement(m.getParameterType().name());
					writer.writeAttribute("Name", m.getP_name());

					if ("date_time".equals(m.getParameterType().name())) {
						Date dt = (Date) m.returnValue();
						writer.writeAttribute("Value",
								getTimestamp((Date) m.returnValue()));
					} else {
						writer.writeAttribute("Value",
								String.valueOf(m.returnValue()));
					}
					writer.writeEndElement();
				}
			}

			if (ds.name != null) {
				writer.writeStartElement("DataSetName");
				writer.writeAttribute("Value", ds.name);
				writer.writeEndElement();
			}

			if (ds.date != null) {
				writer.writeStartElement("DataSetDate");
				writer.writeAttribute("Value", getTimestamp(ds.date));
				writer.writeEndElement();
			}

			if (ds.project != null) {
				writer.writeStartElement("DataSetProject");
				writer.writeAttribute("Value", ds.project);
				writer.writeEndElement();
			}

//			if (ds.task != null) {
//				writer.writeStartElement("DataSetTask");
//				writer.writeAttribute("Value", ds.task);
//				writer.writeEndElement();
//			}
//
//			if (ds.source != null) {
//				writer.writeStartElement("DataSetSource");
//				writer.writeAttribute("Value", ds.source);
//				writer.writeEndElement();
//			}

			if (ds.id != null) {
				writer.writeStartElement("DataSetId");
				writer.writeAttribute("Value", String.valueOf(ds.id));
				writer.writeEndElement();
			}

			if (!FileUtils.isNullOrEmpty(ds.deviceToken)) {
				writer.writeStartElement("DataSetDeviceToken");
				writer.writeAttribute("Value", ds.deviceToken);
				writer.writeEndElement();
			}

			if (isStartStream) {
				writer.writeStartElement("DataSetStartStream");
				writer.writeAttribute("Value", "true");
				writer.writeEndElement();
			}

			if (!isStartStream && (ds.parentDatasetId != null)) {
				writer.writeStartElement("DataSetParentId");
				writer.writeAttribute("Value", String.valueOf(ds.parentDatasetId));
				writer.writeEndElement();
			}

			if (ds.isStreamType) {
				writer.writeStartElement("StreamIndex");
				writer.writeAttribute("Value", String.valueOf(ds.streamIndex));
				writer.writeEndElement();
			}

			writer.writeStartElement("Done");
			writer.writeCharacters(finalWrite ? "1" : "0");
			writer.writeEndElement();

			writer.writeStartElement("Hash");

			String hash = "";
			try {
				hash = calculateMD5Hash(metaPath.replace(".lgmf", ".lgdf"));
			} catch (NoSuchAlgorithmException e) {
				// Nothing to do here
			}
			writer.writeCharacters(hash);
			writer.writeEndElement();

			writer.writeStartElement("Imported");
			writer.writeCharacters("0");
			writer.writeEndElement();

			writer.writeEndDocument();
			writer.close();
		} catch (XMLStreamException | IOException e) {
			// Do nothing
		}
		return sw.toString();
	}

	private String calculateMD5Hash(String path) throws IOException,
			NoSuchAlgorithmException {

		MessageDigest md = null;
		md = MessageDigest.getInstance("MD5");
		byte[] buffer = new byte[8192];
		int read = 0;
		try (InputStream is = Files.newInputStream(Paths.get(path))) {
			// DigestInputStream dis = new DigestInputStream(is, md);
			while ((read = is.read(buffer)) > 0) {
				md.update(buffer, 0, read);
			}
		}
		byte[] digest = md.digest();
		String hash = toHex(digest);
		return hash;
	}

	static String toHex(byte[] data) {
		char[] chars = new char[data.length * 2];
		for (int i = 0; i < data.length; i++) {
			chars[i * 2] = HEX_DIGITS[(data[i] >> 4) & 0xf];
			chars[i * 2 + 1] = HEX_DIGITS[data[i] & 0xf];
		}
		return new String(chars);
	}

	static String byteArrayToHexString(byte[] array) {
		StringBuffer hexString = new StringBuffer();
		for (byte b : array) {
			int intVal = b & 0xff;
			if (intVal < 0x10)
				hexString.append("0");
			hexString.append(Integer.toHexString(intVal));
		}
		return hexString.toString();
	}

	private String getTimestamp(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
		String ret = sdf.format(date);
		return ret;
	}

	// / <summary>
	// / PeriodicWrite and Close, replace the Write(string filePath) function
	// / Now every N items the dataset data is written to a file.
	// / At the end of the capture operation Close is called. This writes the
	// remaining N-X items to the file and appends the XML structure + dataset
	// meta data string.
	// / Call this from item add every N items.
	// / </summary>
	// / <returns></returns>
	void periodicWrite() throws IOException {

		if (firstWrite) {
			createNewLgf();
			firstWrite = false;
		}

		try (FileOutputStream fileStream = new FileOutputStream(filePath, true)) {

			DataSet ds = this;
			ArrayList<PCBase> pars = runningList;

			Collections.sort(pars, new Comparator<PCBase>() {
				@Override
				public int compare(PCBase pcb1, PCBase pcb2) {
					int ret = 0;
					if (pcb1.getParameterIndex() > pcb2.getParameterIndex()) {
						ret = 1;
					} else {
						ret = -1;
					}
					return ret;
				}
			});

			int paramCount = pars.size();
			int itemCount = ds.getMaxParamCollectionCount();

			String s = "";
			for (Integer i = 0; i < itemCount; i++) {
				s = "";
				for (Integer paramCol = 0; paramCol < paramCount; paramCol++) {
					s += ((ParameterCollection<?>) pars.toArray()[paramCol])
							.StringValueAt(i);
					s += ",";
				}
				s = s.substring(0, s.length() - 1);
				s += "\r\n";
				fileStream.write(s.getBytes());

			}
		}
		clearCollections();
	}

	private void createNewLgf() throws IOException {

		String fPath = FileUtils.isNullOrEmpty(this.getFolderPath()) ? FileUtils
				.getFolderPath() : this.getFolderPath();
		File lgf = new File(fPath, createFileName());
		filePath = lgf.getPath();
		fileStream = null;
		if (!lgf.exists()) {
			lgf.createNewFile();
		}

	}

	/**
	 * Call this method on Stream-type Datasets to write to file everything that
	 * has been gathered to this point. Useful to get access to Data that is
	 * being captured over a long time period. Once called, the data will be
	 * available for viewing in Looking Glass.
	 * 
	 * @throws LookingGlassException
	 *             Stream can only be called on Stream-type Datasets. Pass in a
	 *             Boolean of value True to the Dataset constructor to create a
	 *             Stream-type Dataset.
	 * @throws IOException If there is an error writing to file
	 * @throws NoSuchAlgorithmException If there is an error with file hash
	 * 
	 */
	public String stream() throws LookingGlassException, IOException,
			NoSuchAlgorithmException, XMLStreamException {
		streamCalled = true;
		if (!isStreamType && !isCloudType) {
			throw new LookingGlassException(
					"Stream can only be called on Stream-type Datasets. Pass in a Boolean of value True to the Dataset constructor to create a Stream-type Dataset.");
		}
		periodicWrite();
		writeLGMF();

		metaPath = filePath.replace(".lgdf", ".lgmf");
		if (isCloudType
				&& !FileUtils.isFileEmpty(filePath)
				&& MultipartFileUploader.upload(metaPath, CLOUD_URL
						+ "/datasets/lgf", cloudUserName, cloudPassword)) {
			Files.delete(Paths.get(metaPath));
			Files.delete(Paths.get(filePath));

		}

		createNewLgf();
		date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//		String formattedDate = sdf.format(date);
//		String randomToken = createToken();
		//this.setSelfGlobalId("API" + "_" + randomToken + "_" + formattedDate);
		isStartStream = false;
		if (isStreamType) {
			streamIndex++;
		}
		return filePath;
	}

	/**
	 * 
	 * Writes any remaining items/parameters to file and finalises the DataSet
	 * object. Should be called once at the end of a Data-Set. If Cloud
	 * Credentials were passed to the constructor, the Dataset will be uploaded
	 * and deleted locally (on successful upload).
	 * 
	 * @return The gloabl_id of the Dataset if it was uploaded to the cloud, or
	 *         else the path to the file path where the Dataset is written to
	 *         locally.
	 * 
	 * 
	 * @throws IOException If there is an error writing to file
	 * @throws XMLStreamException If there is an error writing to file
	 * @throws NoSuchAlgorithmException If there is an error writing to file
	 * @throws LookingGlassException Internal error
	 */
	public String endWrite() throws IOException, XMLStreamException,
			NoSuchAlgorithmException, LookingGlassException {

		if (isStreamType) {
			finalWrite = true;
			if (!streamCalled) {
				throw new LookingGlassException(
						"Stream() must be called at least once before calling EndWrite() on a Stream-type Dataset.");
			}
		}
		periodicWrite();
		writeLGMF();
		String metaPath = filePath.replace(".lgdf", ".lgmf");
		String ret = "";
		if (isCloudType
				&& !FileUtils.isFileEmpty(filePath)
				&& MultipartFileUploader.upload(metaPath, CLOUD_URL
						+ "/datasets/lgf", cloudUserName, cloudPassword)) {
			if (this.getParentDatasetId() == null) {
				ret = String.valueOf(this.id);
			} else {
				ret = String.valueOf(this.getParentDatasetId());
			}
			Files.delete(Paths.get(metaPath));
			Files.delete(Paths.get(filePath));

		} else {
			ret = metaPath;
		}
		return ret;
	}

	void writeLGMF() throws NoSuchAlgorithmException, XMLStreamException,
			IOException {
		metaPath = filePath.replace(".lgdf", ".lgmf");
		File lgf = new File(metaPath);
		fileStream = null;
		if (!lgf.exists()) {
			lgf.createNewFile();

		}
		fileStream = new FileOutputStream(metaPath, true);
		String s = "";
		s = generateMetaString(this);
		if (s != null && s != "") {
			fileStream.write((s.getBytes()));
		}

		fileStream.close();
		fileStream = null;
	}

	/**
	 * Adds a date type parameter to the current item. A new item can be
	 * switched to by calling "endItem()"
	 * 
	 * @param name
	 *            The name of the parameter
	 * @param value
	 *            The value of the parameter
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 */
	public void addParameterDate(String name, Date value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		if (parameterNames.contains(name)) {

			boolean found = false;
			for (ParameterCollection<Date> pc : parameterCollectionDate) {
				if (pc.getP_name().equals(name)) {
					pc.getP_values().add(new Pair<Boolean, Date>(true, value));
					pc.setNewItemThisRound(true);
					found = true;
				}
			}
			if (!found) {
				throw new LookingGlassException(DUPLICATE_PARAMETER_ERROR);
			}

		} else {
			ParameterCollection<Date> pCNew = new ParameterCollection<Date>(
					name, LookingGlassType.date_time, parameterIndex);
			parameterIndex++;
			for (int i = 0; i < itemCount; i++) {
				pCNew.PadValue();
			}
			parameterCollectionDate.add(pCNew);
			runningList.add(pCNew);
			pCNew.getP_values().add(new Pair<Boolean, Date>(true, value));
			pCNew.setNewItemThisRound(true);
			parameterNames.add(name);
		}
	}

	/**
	 * Adds a string type parameter to the current item. A new item can be
	 * switched to by calling "endItem()"
	 * 
	 * @param name
	 *            The name of the parameter
	 * @param value
	 *            The value of the parameter
	 * 
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 */
	public void addParameterString(String name, String value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		if (parameterNames.contains(name)) {

			// if(parameterCollectionDate.){
			boolean found = false;

			for (ParameterCollection<String> pc : parameterCollectionString) {
				if (pc.getP_name().equals(name)) {
					pc.getP_values()
							.add(new Pair<Boolean, String>(true, value));
					pc.setNewItemThisRound(true);
					found = true;
				}

			}
			if (!found) {
				throw new LookingGlassException(DUPLICATE_PARAMETER_ERROR);
			}
		} else {
			ParameterCollection<String> pCNew = new ParameterCollection<String>(
					name, LookingGlassType.string_value, parameterIndex);
			parameterIndex++;
			for (int i = 0; i < itemCount; i++) {
				pCNew.PadValue();
			}
			parameterCollectionString.add(pCNew);
			runningList.add(pCNew);
			pCNew.getP_values().add(new Pair<Boolean, String>(true, value));
			pCNew.setNewItemThisRound(true);
			parameterNames.add(name);
		}
	}

	/**
	 * Adds a integer type parameter to the current item. A new item can be
	 * switched to by calling "endItem()"
	 * 
	 * @param name
	 *            The name of the parameter
	 * @param value
	 *            The value of the parameter
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 */
	public void addParameterInt(String name, int value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		if (parameterNames.contains(name)) {

			// if(parameterCollectionDate.){
			boolean found = false;

			for (ParameterCollection<Integer> pc : parameterCollectionInteger) {
				if (pc.getP_name().equals(name)) {
					pc.getP_values().add(
							new Pair<Boolean, Integer>(true, value));
					pc.setNewItemThisRound(true);
					found = true;
				}

			}
			if (!found) {
				throw new LookingGlassException(DUPLICATE_PARAMETER_ERROR);
			}
		} else {
			ParameterCollection<Integer> pCNew = new ParameterCollection<Integer>(
					name, LookingGlassType.integer, parameterIndex);
			parameterIndex++;
			for (int i = 0; i < itemCount; i++) {
				pCNew.PadValue();
			}
			parameterCollectionInteger.add(pCNew);
			runningList.add(pCNew);
			pCNew.getP_values().add(new Pair<Boolean, Integer>(true, value));
			pCNew.setNewItemThisRound(true);
			parameterNames.add(name);
		}
	}

	/**
	 * Adds a double type parameter to the current item. A new item can be
	 * switched to by calling "endItem()"
	 * 
	 * @param name
	 *            The name of the parameter
	 * @param value
	 *            The value of the parameter
	 * @throws LookingGlassException
	 *             if name is greater than 255 characters
	 */
	public void addParameterDouble(String name, double value)
			throws LookingGlassException {

		if (name.length() > 255) {
			throw new LookingGlassException(NAME_LENGTH_ERROR);
		}

		if (parameterNames.contains(name)) {

			// if(parameterCollectionDate.){
			boolean found = false;

			for (ParameterCollection<Double> pc : parameterCollectionDouble) {
				if (pc.getP_name().equals(name)) {
					pc.getP_values()
							.add(new Pair<Boolean, Double>(true, value));
					pc.setNewItemThisRound(true);
					found = true;
				}

			}
			if (!found) {
				throw new LookingGlassException(DUPLICATE_PARAMETER_ERROR);
			}
		} else {
			ParameterCollection<Double> pCNew = new ParameterCollection<Double>(
					name, LookingGlassType.double_precision, parameterIndex);
			parameterIndex++;
			for (int i = 0; i < itemCount; i++) {
				pCNew.PadValue();
			}
			parameterCollectionDouble.add(pCNew);
			runningList.add(pCNew);
			pCNew.getP_values().add(new Pair<Boolean, Double>(true, value));
			pCNew.setNewItemThisRound(true);
			parameterNames.add(name);
		}
	}

	/**
	 * @param csvPath
	 *            Path to where the .csv will be written
	 * 
	 *            Write the Dataset to file in .csv format
	 * 
	 * @throws FileNotFoundException
	 *             Problem with csvPath parameter
	 */
	public void writeCSV(String csvPath) throws FileNotFoundException,
			IOException {
		File lgCsv = new File(csvPath);
		fileStream = null;
		if (!lgCsv.exists()) {
			try {
				lgCsv.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try (FileOutputStream fileStream = new FileOutputStream(csvPath, true)) {
			String s = "";
			String lineData = "";
			s = generateParamString(this);
			if (s != null && s != "") {
				fileStream.write((s.getBytes()));
			}
			File lgdfFile = new File(filePath);

			if (lgdfFile.exists()) {
				try (BufferedReader br = new BufferedReader(new FileReader(
						filePath))) {
					fileStream.write(System.getProperty("line.separator")
							.getBytes());
					while ((lineData = br.readLine()) != null) {
						lineData += System.getProperty("line.separator");
						fileStream.write(lineData.getBytes());
					}
				}
			}
			fileStream.close();
		}

	}

	private String generateParamString(DataSet dataSet) {
		String output = "";
		ArrayList<PCBase> allParams = dataSet.getAllParams();
		Collections.sort(allParams, new Comparator<PCBase>() {
			@Override
			public int compare(PCBase param1, PCBase param2) {

				return Integer.compare(param2.getParameterIndex(),
						param1.getParameterIndex());
			}
		});

		if (allParams.size() > 0) {
			for (PCBase pc : allParams) {
				if (allParams.get(allParams.size() - 1) != pc) {
					output += (pc.getP_name() + ",");
				} else {
					output += (pc.getP_name());
				}
			}
		}
		return output;
	}

	// private boolean doesParamAlreadyExist(String name, LookingGlassType
	// lgType) {
	// boolean ret = false;
	// for (Pair<String, LookingGlassType> p : this.getParameterNameTypeList())
	// {
	// if (p.getFirst().equals(name) && p.getSecond().equals(lgType)) {
	// ret = true;
	// }
	// }
	// return ret;
	// }

}
