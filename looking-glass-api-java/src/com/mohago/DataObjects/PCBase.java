package com.mohago.DataObjects;

class PCBase {
	
	 private String p_name;

     private LookingGlassType parameterType;
     private int parameterIndex = -1;
     
     private int Count; //{ get { return -1; } }

     private int MaxIndex; //{ get { return -1; } }
     
     void ClearValues()
     {

     }

     Object ValueAt(int index)
     {
         return null;
     }
     String StringValueAt(int index)
     {
         return "";
     }

     void PadValue() { }
     void AddValue(int index,Object newValue)
     {
         
     }
     
     private boolean newItemThisRound = false;
     
     /// <summary>
     /// converts from IndexedValues to padded Values
     /// </summary>
     void IndexedToPadded(int padTo){

     }

     void PaddedToIndexed()
     {

     }


	String getP_name() {
		return p_name;
	}


	void setP_name(String p_name) {
		this.p_name = p_name;
	}


	LookingGlassType getParameterType() {
		return parameterType;
	}


	void setParameterType(LookingGlassType parameterType) {
		this.parameterType = parameterType;
	}


	int getParameterIndex() {
		return parameterIndex;
	}


	void setParameterIndex(int parameterIndex) {
		this.parameterIndex = parameterIndex;
	}
	int getCount() {
		//return Count;
		return -1;
	}
	void setCount(int count) {
		Count = count;
	}
	int getMaxIndex() {
		//return MaxIndex;
		return -1;
	}
	void setMaxIndex(int maxIndex) {
		MaxIndex = maxIndex;
	}
	boolean isNewItemThisRound() {
		return newItemThisRound;
	}
	void setNewItemThisRound(boolean newItemThisRound) {
		this.newItemThisRound = newItemThisRound;
	}

}
