package com.mohago.DataObjects;

class PBase {
	
	private String p_name;

    private LookingGlassType parameterType;

    private int parameterIndex = -1;

	String getP_name() {
		return p_name;
	}

	void setP_name(String p_name) {
		this.p_name = p_name;
	}

	LookingGlassType getParameterType() {
		return parameterType;
	}

	void setParameterType(LookingGlassType parameterType) {
		this.parameterType = parameterType;
	}

	int getParameterIndex() {
		return parameterIndex;
	}

	void setParameterIndex(int parameterIndex) {
		this.parameterIndex = parameterIndex;
	}

}
