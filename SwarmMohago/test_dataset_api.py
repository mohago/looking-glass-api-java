import requests
import unittest

import json



class MohagoFunctionalSuite(unittest.TestCase):
    
    client = requests.Session()
    client.verify = False
    dataset = {}
    MOHAGO_URL = 'http://vm2.camachoapi.com/v1'
    
    """ Helper Methods """
    
    
    def setUp(self):
        """ on_start is called when a Locust start before any task is scheduled """
        resp = self.login()
        self.auth_token = resp.headers.get("x-subject-token")
        self.client.headers.update({'X-Auth-Token': self.auth_token, 'content-type': 'application/json'})
        self.dataset = self.postDataset();
        
    def tearDown(self):
        self.delete_dataset()
        
    
    """ Login Methods """
        

    def login(self):
        return self.client.post(self.MOHAGO_URL + "/authenticate", headers={"user_name":"dylan_buckley@hotmail.com", "password":"123"} )
    
    
    """ Dataset Methods """

    def postDataset(self):
        with open ("c:\users\dylan\desktop\clear\cleanup\dataset-json.txt", "r") as myfile:
            my_data=myfile.read().replace('\n', '')
        resp = self.client.post(self.MOHAGO_URL + "/datasets", json = json.loads(my_data))
        self.dataset = json.loads(resp.text)
        self.dataset = self.get_dataset()
        return self.dataset
    
    def delete_dataset(self):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'])
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def get_dataset(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'])
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def list_datasets(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets")
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Parameter Methods """
        
    def get_parameters(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + '/parameters')
        temp = json.loads(resp.text)
        print temp
        return temp
        
    def get_parameter_values(self, param_name):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + '/parameters/' + param_name)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    
    """ Items Methods """
        
    def post_items(self, json_items):
        resp = self.client.post(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/items", json = json_items)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Meta Methods """
    
    def get_meta(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta")
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def add_meta(self, new_meta_array):
        json_body = new_meta_array
        resp = self.client.post(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta", json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def update_meta(self, new_meta_array):
        json_body = new_meta_array
        resp = self.client.put(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta", json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def update_meta_element(self, meta_name, new_value):
        json_body = {"value": new_value}
        resp = self.client.put(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta/" + meta_name, json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def delete_meta_element(self, meta_name):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta/" + meta_name)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    
    def test_listDataset(self):
        resp = self.list_datasets()
        print resp
        
    def test_addItems(self):

        resp = self.get_parameters()
        self.assertEqual(resp['a'], 0, 'Wrong type')
        self.assertEqual(resp['b'], 0, 'Wrong type')
        resp = self.get_parameter_values('a')
        self.assertEqual(resp['content'][0], [0, 1], 'Parameter values incorrect');
        
        with open ("c:\users\dylan\desktop\clear\cleanup\multiple-items.txt", "r") as myfile:
            my_data=myfile.read().replace('\n', '')
        self.post_items(json.loads(my_data))
        
        resp = self.get_parameter_values('a')
        self.assertEqual(resp['content'][5], [5, 6], 'Parameter values incorrect');
        #7
        with open ("c:\users\dylan\desktop\clear\cleanup\single-item.txt", "r") as myfile:
            my_data=myfile.read().replace('\n', '')
        self.post_items(json.loads(my_data))
        resp = self.get_parameter_values('a')
        self.assertEqual(resp['content'][6], [6, 7], 'Parameter values incorrect')
        self.get_dataset()
        
        
    def test_readDataset(self):
        resp = self.get_dataset()
    
    def test_readMeta(self):
        resp = self.get_meta()
    
    def test_meta(self):
        resp = self.get_meta()
        self.assertEqual(resp['m_a'], 1, 'Meta incorrect')
        self.update_meta_element('m_a', 2)
        resp = self.get_meta()
        self.assertEqual(resp['m_a'], 2, 'Meta incorrect')
        meta2 = [
            {
                "name": "m_a_2",
                "value": -1
            }
        ]
        self.update_meta(meta2)
        resp = self.get_meta()
        self.assertEqual(resp['m_a_2'], -1, 'Meta incorrect')
        
        resp = self.get_meta()
        
        new_meta_data = [{
            "name": "m_b_2",
            "value": 17.8
        }]
        self.add_meta(new_meta_data)
        resp = self.get_meta()
        self.assertEqual(resp['m_a_2'], -1, 'Meta incorrect')
        self.assertEqual(resp['m_b_2'], 17.8, 'Meta incorrect')
        
        self.delete_meta_element('m_a_2')
        resp = self.get_meta()
        try :
            temp = resp['m_a_2']
            self.fail('Shouldn\'t be reached')
        except KeyError, e:
            self.assertIsNotNone(e, 'Exception thrown')
            
        #update
        
    def test_updateMeta(self):
        
        #self.postDataset();
        new_data = {
            "value": 'new'
        }
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta" + "/" + "meta_1", json = new_data)
        

    def test_getParameters(self):
        #self.postDataset();
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/parameters")
        
    def test_getValues(self):
        #self.postDataset();
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/parameters/a")
    
    def test_getChartPoints(self):
        #self.postDataset();
        resp = self.client.get(self.MOHAGO_URL + "/chart/v2", params={'datasetId':self.dataset['datasetId'], 'parameters':'a,b'})
        
    
    
    

    