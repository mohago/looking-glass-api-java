import requests
import json



class CamachoApiWrapper():
    
    client = requests.Session()
    client.verify = False
    dataset = {}
    MOHAGO_URL = 'https://vm2.camachoapi.com/v1'
    
    """ Helper Methods """
    
    
    def setUp(self):
        """ on_start is called when a Locust start before any task is scheduled """
        resp = self.login()
        self.auth_token = resp.headers.get("x-subject-token")
        self.client.headers.update({'X-Auth-Token': self.auth_token, 'content-type': 'application/json'})
        self.dataset = self.postDataset();
        
    def tearDown(self):
        self.delete_dataset()
        
    
    """ Login Methods """
        

    def login(self):
        return self.client.post(self.MOHAGO_URL + "/authenticate", headers={"user_name":"dylan_buckley@hotmail.com", "password":"123"} )
    
    
    """ Dataset Methods """

    def postDataset(self):
        with open ("c:\users\dylan\desktop\clear\cleanup\dataset-json.txt", "r") as myfile:
            my_data=myfile.read().replace('\n', '')
        resp = self.client.post(self.MOHAGO_URL + "/datasets", json = json.loads(my_data))
        self.dataset = json.loads(resp.text)
        self.dataset = self.get_dataset()
        return self.dataset
    
    def delete_dataset(self):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'])
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def get_dataset(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'])
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def list_datasets(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets")
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Parameter Methods """
        
    def get_parameters(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + '/parameters')
        temp = json.loads(resp.text)
        print temp
        return temp
        
    def get_parameter_values(self, param_name):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + '/parameters/' + param_name)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    
    """ Items Methods """
        
    def post_items(self, json_items):
        resp = self.client.post(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/items", json = json_items)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Meta Methods """
    
    def get_meta(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta")
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def add_meta(self, new_meta_array):
        json_body = new_meta_array
        resp = self.client.post(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta", json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def update_meta(self, new_meta_array):
        json_body = new_meta_array
        resp = self.client.put(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta", json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def update_meta_element(self, meta_name, new_value):
        json_body = {"value": new_value}
        resp = self.client.put(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta/" + meta_name, json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    def delete_meta_element(self, meta_name):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta/" + meta_name)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    

    