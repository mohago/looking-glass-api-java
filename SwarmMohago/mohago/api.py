# python
from __future__ import unicode_literals

# libs

# local
from .base import APIClient


# LookingGlass
class LookingGlass(object):
    authenticate = APIClient(service_uri='authenticate/')
    chart = APIClient(service_uri='chart/')
    datasets = APIClient(service_uri='datasets/')
    #datasets_top_level = APIClient(service_uri='datasets/request/top-level/')
    datasets_items = APIClient(service_uri='datasets/{globalId}/items/')
    datasets_meta = APIClient(service_uri='datasets/{globalId}/meta/')
    datasets_meta_element = APIClient(service_uri='datasets/{globalId}/meta/{name}/')
    datasets_parameters = APIClient(
        service_uri='datasets/{globalId}/parameters/')
    datasets_values = APIClient(
        service_uri='datasets/{globalId}/parameters/{name}/')
