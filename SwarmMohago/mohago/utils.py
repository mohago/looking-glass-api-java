# python
from __future__ import unicode_literals
import importlib
import os


__all__ = ['settings']


def new_method_proxy(func):
    """When attribute is accessed in lazy object, this method makes sure that
    lazy object was properly initialized and _setup method has been run
    """
    def inner(self, *args):
        if self._wrapped is None:
            self._setup()
        return func(self._wrapped, *args)
    return inner


class LazySettings(object):
    """Lazy settings module. We want settings to be imported when they are
    accessed not earlier.
    """
    _wrapped = None
    __getattr__ = new_method_proxy(getattr)

    def _setup(self):
        """
        Load the settings module pointed to by the environment variable. This
        is used the first time we need any settings at all, if the user has not
        previously configured the settings manually.
        """
        try:
            settings_module = os.environ['CLOUDCIX_SETTINGS_MODULE']
            if not settings_module:  # If it's set but is an empty string.
                raise KeyError
        except KeyError:
            raise ImportError("You must specify the CLOUDCIX_SETTINGS_MODULE "
                              "environment variable.")
        else:
            settings_module = settings_module.split(":")
            self._wrapped = importlib.import_module(settings_module[0])
            if len(settings_module) > 1:
                self._wrapped = getattr(self._wrapped, settings_module[1])


settings = LazySettings()
