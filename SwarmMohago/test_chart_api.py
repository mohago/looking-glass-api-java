import requests
import unittest
import urllib

import json



class MohagoChartFunctionalSuite(unittest.TestCase):
    
    client = requests.Session()
    client.verify = False
    dataset_id_list = []
    dataset = {}

    MOHAGO_URL = 'http://vm2.camachoapi.com/v1'
    
    """ Helper Methods """
    
    @classmethod
    def setUpClass(self):
        """ on_start is called when a Locust start before any task is scheduled """
        resp = self.login()
        self.auth_token = resp.headers.get("x-subject-token")
        self.client.headers.update({'X-Auth-Token': self.auth_token, 'content-type': 'application/json'})
        self.dataset = self.postDataset("c:\users\dylan\desktop\clear\cleanup\dataset-chart-json.txt")

    
    @classmethod    
    def tearDownClass(self):
        self.delete_dataset(self.dataset['datasetId'])
        
    
    """ Login Methods """
        
    @classmethod 
    def login(self):
        return self.client.post(self.MOHAGO_URL + "/authenticate", headers={"user_name":"dylan_buckley@hotmail.com", "password":"123"} )
    
    
    """ Dataset Methods """
    @classmethod 
    def postDataset(self, filePath):
        with open (filePath, "r") as myfile:
            my_data=myfile.read().replace('\n', '')
        resp = self.client.post(self.MOHAGO_URL + "/datasets", json = json.loads(my_data))
        dataset = json.loads(resp.text)
        dataset = self.get_dataset(dataset['datasetId'])
        return dataset
    
    @classmethod 
    def delete_dataset(self, datasetId):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + datasetId)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def get_dataset(self, datasetId):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + datasetId)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Chart Methods """
    
    @classmethod 
    def get_chart_data(self, chartDict):
        resp = self.client.get(self.MOHAGO_URL + "/chart" + "/" + self.dataset['datasetId'], params=chartDict)
        temp = json.loads(resp.text)
        print temp
        return temp
    

    def test_get_chart_data(self):
        chartDict = dict(parameters="a,b")
        resp = self.get_chart_data(chartDict)
        self.assertEqual(3, len(resp['data']))
        
   

        
        
        
    
    
    

    