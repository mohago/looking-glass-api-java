import requests
import unittest
import urllib

import json



class MohagoSearchFunctionalSuite(unittest.TestCase):
    
    client = requests.Session()
    client.verify = False
    dataset_id_list = []
    dataset1 = {}
    dataset2 = {}
    dataset3 = {}
    dataset4 = {}
    dataset5 = {}
    MOHAGO_URL = 'http://vm2.camachoapi.com/v1'
    
    """ Helper Methods """
    
    @classmethod
    def setUpClass(self):
        """ on_start is called when a Locust start before any task is scheduled """
        resp = self.login()
        self.auth_token = resp.headers.get("x-subject-token")
        self.client.headers.update({'X-Auth-Token': self.auth_token, 'content-type': 'application/json'})
        self.dataset1 = self.postDataset("c:\users\dylan\desktop\clear\cleanup\dataset-json-1.txt")
        self.dataset2 = self.postDataset("c:\users\dylan\desktop\clear\cleanup\dataset-json-2.txt")
        self.dataset3 = self.postDataset("c:\users\dylan\desktop\clear\cleanup\dataset-json-3.txt")
        self.dataset4 = self.postDataset("c:\users\dylan\desktop\clear\cleanup\dataset-json-4.txt")
        self.dataset5 = self.postDataset("c:\users\dylan\desktop\clear\cleanup\dataset-json-5.txt")
        self.dataset_id_list.append(self.dataset1['datasetId'] )
        self.dataset_id_list.append(self.dataset2['datasetId'] )
        self.dataset_id_list.append(self.dataset3['datasetId'] )
        self.dataset_id_list.append(self.dataset4['datasetId'] )
        self.dataset_id_list.append(self.dataset5['datasetId'] )
    
    @classmethod    
    def tearDownClass(self):
        self.delete_dataset(self.dataset1['datasetId'])
        self.delete_dataset(self.dataset2['datasetId'])
        self.delete_dataset(self.dataset3['datasetId'])
        self.delete_dataset(self.dataset4['datasetId'])
        self.delete_dataset(self.dataset5['datasetId'])
        
    
    """ Login Methods """
        
    @classmethod 
    def login(self):
        return self.client.post(self.MOHAGO_URL + "/authenticate", headers={"user_name":"dylan_buckley@hotmail.com", "password":"123"} )
    
    
    """ Dataset Methods """
    @classmethod 
    def postDataset(self, filePath):
        with open (filePath, "r") as myfile:
            my_data=myfile.read().replace('\n', '')
        resp = self.client.post(self.MOHAGO_URL + "/datasets", json = json.loads(my_data))
        dataset = json.loads(resp.text)
        dataset = self.get_dataset(dataset['datasetId'])
        return dataset
    
    @classmethod 
    def delete_dataset(self, datasetId):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + datasetId)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def get_dataset(self, datasetId):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + datasetId)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def list_datasets(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets")
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Meta Methods """
    
    @classmethod 
    def get_meta(self):
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta")
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def add_meta(self, new_meta_array):
        json_body = new_meta_array
        resp = self.client.post(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta", json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def update_meta(self, new_meta_array):
        json_body = new_meta_array
        resp = self.client.put(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta", json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def update_meta_element(self, meta_name, new_value):
        json_body = {"value": new_value}
        resp = self.client.put(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta/" + meta_name, json = json_body)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def delete_meta_element(self, meta_name):
        resp = self.client.delete(self.MOHAGO_URL + "/datasets" + "/" + self.dataset['datasetId'] + "/meta/" + meta_name)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    """ Search Methods """
    
    @classmethod 
    def search_by_id_list(self, dataset_id_list):
        dataset_id_list_str = ''
        if ((dataset_id_list) and len(dataset_id_list) > 0):
            for id in dataset_id_list:
                dataset_id_list_str += id + ','
            dataset_id_list_str = dataset_id_list_str[:-1]
        
        resp = self.client.get(self.MOHAGO_URL + "/datasets" + "?datasetIdList=" + dataset_id_list_str)
        temp = json.loads(resp.text)
        print temp
        return temp
    
    @classmethod 
    def search_by_dict(self, searchDict):     
        resp = self.client.get(self.MOHAGO_URL + "/datasets", params=searchDict)
        temp = json.loads(resp.text)
        print temp
        return temp

    def test_search_by_ids(self):
        dataset_id_list = self.dataset_id_list
        dataset_id_list.pop()
        resp = self.search_by_id_list(dataset_id_list)
        self.assertEqual(4, len(resp))
        
    def test_search_by_start_time(self):
        searchDict = dict(startTime="2012-03-05T12:57:02.015", project="search")
        resp = self.search_by_dict(searchDict)
        self.assertEqual(5, len(resp))
        
    def test_search_by_end_time(self):
        searchDict = dict(endTime="2012-03-05T12:57:02.015", project="search")
        resp = self.search_by_dict(searchDict)
        self.assertEqual(404, resp['code'])
        
        
    def test_search_by_name(self):
        searchDict = dict(name="search_2", project="search")
        resp = self.search_by_dict(searchDict)
        self.assertEqual(1, len(resp))
        
    def test_offset(self):
        searchDict = dict(offset=2, project="search")
        resp = self.search_by_dict(searchDict)
        self.assertEqual(3, len(resp))
        
    def test_limit(self):
        searchDict = dict(offset=2, limit=1, project="search")
        resp = self.search_by_dict(searchDict)
        self.assertEqual(1, len(resp))
        
    def test_sort_by(self):
        searchDict = dict(sortBy="-name", limit=1, project="search")
        resp = self.search_by_dict(searchDict)
        self.assertEqual("search_5", resp[0]['name'])
        
    def test_filter_by(self):
        searchDict = dict(filterBy="name", limit=1, project="search")
        resp = self.search_by_dict(searchDict)
        with self.assertRaises(KeyError) as raises:
            temp = resp[0]['project']
         
    def test_meta_search_int(self):
        searchDict = dict(metaQuery="(m_int_a==2)")
        resp = self.search_by_dict(searchDict)
        self.assertEqual("search_2", resp[0]['name'])
        
    def test_meta_search_double(self):
        searchDict = dict(metaQuery="(m_double_a==3.0)")
        resp = self.search_by_dict(searchDict)
        self.assertEqual("search_3", resp[0]['name'])
        
    def test_meta_search_date(self):
        searchDict = dict(metaQuery="(m_date_a==2017-03-05T12:57:02.015)")
        resp = self.search_by_dict(searchDict)
        self.assertEqual("search_5", resp[0]['name'])
        
    def test_meta_search_string(self):
        searchDict = dict(metaQuery="(m_string_a==m_search_2)")
        resp = self.search_by_dict(searchDict)
        self.assertEqual("search_2", resp[0]['name'])
        
    def test_meta_search_complex(self):
        searchDict = dict(metaQuery="(m_date_a<2017-03-05T12:57:02.015) && ((m_double_a>3.0)||(m_int_a==1))")
        resp = self.search_by_dict(searchDict)
        self.assertEqual(2, len(resp))
        

        
        
        
    
    
    

    